﻿using System;
using UnityEngine;

namespace Game
{
    public class GameTimer : MonoBehaviour
    {
        [SerializeField] private TextChanger _timerView;
        
        private float _srartTimerValue;
        private float _timer = 0;

        public event Action OnTimeEnd;

        private void Awake()
        {
            if (_timerView == null) Debug.LogError($"Timer View not setted in {gameObject.name}");

            _timerView.Disable();
            enabled = false;
        }       
        private void Update()
        {
            TimerTick();
        }

        public void StartWith(float value)
        {
            _srartTimerValue = value;
            Restart();
            Continue();

            _timerView.Enable();
        }
        public void AddTime(float value)
        {
            if (_timer != 0f) _timer += value;
        }
        public void Stop() => enabled = false;
        public void Continue() => enabled = true;
        public void Restart() => _timer = _srartTimerValue;
        public void Close()
        {
            Stop();
            _timerView.Disable();
        }

        private void TimerTick()
        {
            _timer -= Time.deltaTime;
            SetTextTimer();

            if (_timer <= 0)
            {
                OnTimeEnd?.Invoke();
                Stop();
            }
        }
        private void SetTextTimer()
        {
            _timerView?.SetText($"{_timer:F1}");
        }
    }
}