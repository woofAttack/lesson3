﻿using UnityEngine;
using UnityEngine.SceneManagement;

namespace Game
{
    public class Level : MonoBehaviour
    {
        [Header("Timer")]
        [SerializeField] private bool _isEnableTimer;
        [SerializeField] private GameTimer _timer;
        [SerializeField] private float _timerValue;

        [Header("Objects")]
        [SerializeField] private Player _player;
        [SerializeField] private Exit _exitFromLevel;

        [Header("UI enjoyer")]
        [SerializeField] private GameResultView _uiResult;

        private bool _gameIsEnded = false;

        private void Awake()
        {
            if (_timer == null)
            {
                Debug.LogError($"Timer not setted in {gameObject.name}");
                _isEnableTimer = false;
            };
        }
        private void Start()
        {
            _exitFromLevel.Close();
            if (_isEnableTimer) _timer.StartWith(_timerValue);
        }

        private void OnEnable()
        {
            if (_isEnableTimer) _timer.OnTimeEnd += Lose;
        }
        private void OnDisable()
        {
            if (_isEnableTimer) _timer.OnTimeEnd -= Lose;
        }

        private void Update()
        {
            if (_gameIsEnded)
                return;

            LookAtPlayerHealth();
            LookAtPlayerInventory();
            TryCompleteLevel();
        }

        private void TryCompleteLevel()
        {
            if (_exitFromLevel.IsOpen == false)
                return;

            var flatExitPosition = new Vector2(_exitFromLevel.transform.position.x, _exitFromLevel.transform.position.z);
            var flatPlayerPosition = new Vector2(_player.transform.position.x, _player.transform.position.z);

            if (flatExitPosition == flatPlayerPosition)
                Victory();
        }

        private void LookAtPlayerHealth()
        {
            if (_player.IsAlive)
                return;

            Lose();
            Destroy(_player.gameObject);
        }

        private void LookAtPlayerInventory()
        {
            if (_player.HasKey)
                _exitFromLevel.Open();
        }

        public void Victory()
        {
            EndGame();
            _uiResult.ShowVictoryPanel();
        }

        public void Lose()
        {
            EndGame();
            _uiResult.ShowLosePanel();
        }

        private void EndGame()
        {
            _gameIsEnded = true;
            _player.Disable();
            _timer.Stop();
        }

        public void RestartLevel()
        {
            var level = SceneManager.GetActiveScene().name;
            SceneManager.LoadScene(level);
        }

        public void GoLevel(int numberLevel)
        {
            SceneManager.LoadScene(numberLevel);
        }
    }
}