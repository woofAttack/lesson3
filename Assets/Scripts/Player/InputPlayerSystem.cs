﻿using UnityEngine;

namespace Game
{
    public class InputPlayerSystem : MonoBehaviour
    {
        [SerializeField] private Level _level;

        private void Awake()
        {
            if (_level == null) _level = this.TryFind<Level>();
        }

        private void Update()
        {
            if (Input.GetKeyDown(KeyCode.R))
                _level.RestartLevel();
        }
    }
}