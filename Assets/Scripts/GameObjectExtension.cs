﻿using UnityEngine;

namespace Game
{
    public static class GameObjectExtension
    {
        public static T TryFind<T>(this Component caller) where T : Component
        {           
            var component = Object.FindObjectOfType<T>();

            caller.ThrowExceptionIfComponentNull(component);

            return component;
        }

        public static T TryGetComponentIn<T>(this Component caller) where T : Component
        {
            var component = caller.GetComponent<T>();

            caller.ThrowExceptionIfComponentNull(component);

            return component;
        }

        private static void ThrowExceptionIfComponentNull<T>(this Object caller, T component) where T : Component
        {
            if (component == null)
                throw new System.Exception($"{typeof(T).Name} component not found for {caller.name}");
        }
    }
}
