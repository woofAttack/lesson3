﻿using System;
using UnityEngine;
using UnityEngine.UI;

namespace Game
{
    [RequireComponent(typeof(Text))]
    public class TextChangerSimple : TextChanger
    {
        [SerializeField] private string _textFormat = "{0}";
        private Text _textComponent;

        private void Awake()
        {
            _textComponent = GetComponent<Text>();
        }

        public override void SetText(string text) 
        {
            _textComponent.text = String.Format(_textFormat, text);
        }
    }


}