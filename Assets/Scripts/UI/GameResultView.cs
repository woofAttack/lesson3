﻿using UnityEngine;
using UnityEngine.UI;

namespace Game
{
    public class GameResultView : MonoBehaviour
    {
        [SerializeField] private GameObject _panelResult;

        [SerializeField] private GameObject _textVictoryGame;
        [SerializeField] private GameObject _textLoseGame;

        [SerializeField] private Button _buttonRetry;
        [SerializeField] private Button _buttonAnotherLevel;

        private void Awake()
        {
            if (_panelResult == null)
                throw new System.Exception();

            if (_buttonAnotherLevel == null)
                throw new System.Exception();

            if (_textVictoryGame == null)
                throw new System.Exception();

            if (_textLoseGame == null)
                throw new System.Exception();

            if (_buttonRetry == null)
                throw new System.Exception();
        }

        private void Start()
        {        
            DisableActivePanels();
        }

        public void DisableActivePanels()
        {
            _panelResult.SetActive(false);
            _textVictoryGame.SetActive(false);
            _textLoseGame.SetActive(false);
            _buttonRetry.gameObject.SetActive(false);
            _buttonAnotherLevel.gameObject.SetActive(false);
        }

        public void ShowVictoryPanel()
        {
            SetActiveResultPanel(_textVictoryGame);
            _buttonAnotherLevel.gameObject.SetActive(true);
        }
        public void ShowLosePanel()
        {
            SetActiveResultPanel(_textLoseGame);
        }

        private void SetActiveResultPanel(GameObject panel)
        {
            _panelResult.SetActive(true);
            panel.SetActive(true);
            _buttonRetry.gameObject.SetActive(true);
        }
    }
}