﻿using UnityEngine;

namespace Game
{
    public abstract class TextChanger : MonoBehaviour
    {
        public virtual void SetText(string text) { }

        public void Enable()
        {
            gameObject.SetActive(true);
        }
        public void Disable()
        {
            gameObject.SetActive(false);
        }
    }
}