﻿using UnityEngine;

namespace Game
{
    public class CameraMover : MonoBehaviour
    {
        [SerializeField] private Camera _camera;
        private Transform _cameraTransform;

        private void Awake()
        {
            if (_camera == null) _camera = this.TryFind<Camera>();
            
            _cameraTransform = _camera.transform;
        }

        public void MoveAtXZ(Vector2 position)
        {
            _cameraTransform.position = new Vector3(
                x: position.x,
                y: _cameraTransform.position.y,
                z: position.y);
        }
    }
}
