﻿using System;
using UnityEngine;

namespace Game
{
    [RequireComponent(typeof(ColliderTriggerHandlerSimple))]
    public class CameraMovePoint : MonoBehaviour
    {
        [SerializeField] private CameraMover _mover;      
        [SerializeField] private Transform _transformObjectForMoveCamera;

        [SerializeField] private float _deltaZ;
        
        private ColliderTriggerHandlerSimple _trigger;
        private Vector2 _positionXZToMove;

        private void Awake()
        {
            if (_mover == null) _mover = this.TryFind<CameraMover>();

            _trigger = GetComponent<ColliderTriggerHandlerSimple>();
            _trigger.SetInvokerType<Player>();

            if (_transformObjectForMoveCamera == null)
            {
                // can use debug warning
                _transformObjectForMoveCamera = gameObject.transform;
            }

            _positionXZToMove = new Vector2(
                x: _transformObjectForMoveCamera.position.x,
                y: _transformObjectForMoveCamera.position.z + _deltaZ);
        }

        private void OnEnable()
        {
            _trigger.OnInvokerTriggerEnter += MoveCamera;
        }

        private void OnDisable()
        {
            _trigger.OnInvokerTriggerEnter -= MoveCamera;
        }

        private protected virtual void MoveCamera()
        {
            _mover.MoveAtXZ(_positionXZToMove);
        }


    }
}
