﻿using System;
using UnityEngine;

namespace Game
{
    // mb we can use decorator pattern for additional method with GameObject, ex. Destroy()
    [RequireComponent(typeof(Collider))]
    public class ColliderTriggerHandlerSimple : MonoBehaviour
    {
        public event Action OnInvokerTriggerEnter;
        public event Action OnInvokerTriggerExit;

        private Type _typeInvoker;

        private void Awake()
        {
            if (_typeInvoker == null)
                enabled = false;
        }

        public void SetInvokerType<T>()
        {
            _typeInvoker = typeof(T);
            enabled = true;
        }

        private void OnTriggerEnter(Collider other)
        {
            if (other.TryGetComponent(_typeInvoker, out _))
            {
                OnInvokerTriggerEnter?.Invoke();
            }
        }
        private void OnTriggerExit(Collider other)
        {
            if (other.TryGetComponent(_typeInvoker, out _))
            {
                OnInvokerTriggerExit?.Invoke();
            }
        }

    }
}