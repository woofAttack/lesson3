﻿using System;
using UnityEngine;
using UnityEngine.Events;

namespace Game
{
    // KISS broken now
    [RequireComponent(typeof(Collider))]
    public class ColliderTriggerHandler : MonoBehaviour
    {
        public event Action OnInvokerTriggerEnter;
        public event Action OnInvokerTriggerExit;

        private Type _typeInvoker;

        private UnityEvent<Component> _actionInvokeOnEnter = new UnityEvent<Component>();
        private UnityEvent<Component> _actionInvokeOnExit = new UnityEvent<Component>();

        private void Awake()
        {
            if (_typeInvoker == null) // (_typeInvoker == null) is crutch against the non-obvious order of "Awake()" calls for classes
                enabled = false;
        }

        public void SetInvokerType<T>() => _typeInvoker = typeof(T);
        public void SetActionInvokerOnEnter<T>(Action<T> action) =>
            SetInvokeAction(_actionInvokeOnEnter, action);
        public void SetActionInvokerOnExit<T>(Action<T> action) =>
            SetInvokeAction(_actionInvokeOnExit, action);

        private void SetInvokeAction<T>(UnityEvent<Component> actionTrigger, Action<T> action)
        {
            SetInvokerType<T>();

            actionTrigger.AddListener((x) =>
            {
                var componentT = x.GetComponent<T>();
                action.Invoke(componentT);
            });

            enabled = true;
        }

        private void OnDisable()
        {
            _actionInvokeOnEnter.RemoveAllListeners();
            _actionInvokeOnExit.RemoveAllListeners();
        }

        private void OnTriggerEnter(Collider other)
        {
            if(other.TryGetComponent(_typeInvoker, out Component component))
            {
                OnInvokerTriggerEnter?.Invoke();
                _actionInvokeOnEnter?.Invoke(component);
            }
        }
        private void OnTriggerExit(Collider other)
        {
            if (other.TryGetComponent(_typeInvoker, out Component component))
            {
                OnInvokerTriggerExit?.Invoke();
                _actionInvokeOnExit?.Invoke(component);
            }
        }
    }
}