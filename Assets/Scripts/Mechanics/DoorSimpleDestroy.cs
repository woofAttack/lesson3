﻿namespace Game
{
    public class DoorSimpleDestroy : Door
    {
        private protected override void OpenProcess()
        {
            Destroy(gameObject);
        }
    }
}

