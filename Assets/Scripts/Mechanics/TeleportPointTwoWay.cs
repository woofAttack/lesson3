﻿using System;
using UnityEngine;

namespace Game
{
    public class TeleportPointTwoWay : TeleportPoint
    {
        [SerializeField] private TeleportPointTwoWay _arrivalPoint;

        private void Awake()
        {
            SafeAwake();

            if (_arrivalPoint == null)
                throw new Exception($"Arrivial point not set in {gameObject.name}");
        }

        private protected override Vector3 GetPositionForTeleport()
        {
            return _arrivalPoint.transform.position;
        }
        private protected override void PostMove()
        {
            _arrivalPoint.SetUnactiveTeleport();
        }
    }
}

        