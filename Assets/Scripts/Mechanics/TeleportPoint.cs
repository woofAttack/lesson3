using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Game
{
    [RequireComponent(typeof(ColliderTriggerHandler))]
    public class TeleportPoint : MonoBehaviour
    {
        public event Action OnTriggeringByPlayer;  // ex. for FX invoke

        [SerializeField] private Transform _transformArrivalPoint;
        private Vector3 _positionToTeleport;

        private ColliderTriggerHandler _trigger;
        private bool _playerWasExitFormPoint = true;

        private void Awake()
        {
            SafeAwake();

            if (_transformArrivalPoint == null)
                throw new Exception($"Arrivial point not set in {gameObject.name}");
        }

        private protected void SafeAwake()
        {
            _trigger = GetComponent<ColliderTriggerHandler>();
        }

        private void OnEnable()
        {
            _trigger.SetActionInvokerOnEnter<Player>(TryMove);
            _trigger.OnInvokerTriggerExit += SetActiveTeleport;
        }
        private void OnDisable()
        {
            _trigger.OnInvokerTriggerExit -= SetActiveTeleport;
        }

        public void Enable()
        {
            SetActiveGameObject(true);
        }
        public void Disable()
        {
            SetActiveGameObject(false);
        }


        private void TryMove(Player player)
        {
            if (_playerWasExitFormPoint == true)
            {
                _positionToTeleport = GetPositionForTeleport();

                Move(player);
                PostMove();

                OnTriggeringByPlayer?.Invoke();
            }
        }

        private protected virtual void PostMove() {}

        private protected virtual Vector3 GetPositionForTeleport()
        {
            return _transformArrivalPoint.position;
        }

        private void Move(Player player)
        {
            player.transform.position = new Vector3(
                x: _positionToTeleport.x,
                y: player.transform.position.y,
                z: _positionToTeleport.z);
        }

        private protected void SetActiveTeleport()
        {
            _playerWasExitFormPoint = true;
        }
        private protected void SetUnactiveTeleport()
        {
            _playerWasExitFormPoint = false;
        }
        private void SetActiveGameObject(bool value)
        {
            gameObject.SetActive(value);
        }
    }
}

        