﻿using System;
using UnityEngine;

namespace Game
{
    [RequireComponent(typeof(ColliderTriggerHandlerSimple))]
    public class ActiveObjectByTrigger : MonoBehaviour
    {
        [SerializeField] private GameObject _objectToActive;
        private ColliderTriggerHandlerSimple _trigger;

        private void Awake()
        {
            if (_objectToActive == null) throw new Exception();
            _objectToActive.SetActive(false);

            _trigger = GetComponent<ColliderTriggerHandlerSimple>();
            _trigger.SetInvokerType<Player>();
        }

        private void OnEnable()
        {
            _trigger.OnInvokerTriggerEnter += Activate;
        }

        private void OnDisable()
        {
            _trigger.OnInvokerTriggerEnter -= Activate;
        }

        private void Activate()
        {
            _objectToActive.SetActive(true);
        }
    }
}

