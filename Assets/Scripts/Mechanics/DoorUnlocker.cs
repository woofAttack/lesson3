using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Game
{
    [RequireComponent(typeof(Collider))]
    public class DoorUnlocker : MonoBehaviour
    {
        [SerializeField] private Door _door;
        [SerializeField] private MaterialChanger _materialChanger;

        private void Awake()
        {
            if (_door == null) throw new Exception($"In {gameObject.name} not set Door to unlock");
            if (_materialChanger == null) throw new Exception($"In {gameObject.name} not set MaterialChanger");

            _materialChanger.Init();
            _materialChanger.ChangeMaterialByState(false);
        }

        private void OnEnable()
        {
            _door.OnOpen += ChangeStateUnlockerToTrue;
        }

        private void OnDisable()
        {
            _door.OnOpen -= ChangeStateUnlockerToTrue;
        }

        private void OnTriggerEnter(Collider other)
        {
            if (other.TryGetComponent<Player>(out _))
            {
                _door.Open();
            }
        }

        private void ChangeStateUnlockerToTrue()
        {
            _materialChanger.ChangeMaterialByState(true);
            enabled = false;
        }

    }
}

