﻿using System;
using UnityEngine;

namespace Game
{
    public abstract class Door : MonoBehaviour
    {
        public event Action OnOpen;

        public void Open() 
        {
            OnOpen?.Invoke();
            OpenProcess();
        }

        private protected abstract void OpenProcess();
    }
}

