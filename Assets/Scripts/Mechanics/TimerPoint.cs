﻿using System;
using UnityEngine;

namespace Game
{
    [RequireComponent(typeof(Collider))]
    public class TimerPoint : MonoBehaviour
    {
        public event Action<float> OnTimerPointCollected;

        [SerializeField] private GameTimer _timer;
        [SerializeField] private float _addTime;

        private void OnTriggerEnter(Collider other)
        {
            if (other.TryGetComponent<Player>(out _))
            {
                _timer.AddTime(_addTime);
                OnTimerPointCollected?.Invoke(_addTime);

                Destroy(gameObject);
            }
        }
    }

}