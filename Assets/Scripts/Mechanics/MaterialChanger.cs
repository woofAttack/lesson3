﻿using System;
using UnityEngine;

namespace Game
{
    [RequireComponent(typeof(MeshRenderer))]
    public class MaterialChanger : MonoBehaviour
    {
        [SerializeField] private Material _falseState;
        [SerializeField] private Material _trueState;

        private MeshRenderer _renderer;
        private Material[] _materials = new Material[2];

        private void Awake()
        {
            // wrong order Awake()
            // Door Unlocker faster
        }

        public void Init()
        {
            if (_falseState == null) throw new Exception($"False State Matearial not sets");
            if (_trueState == null) throw new Exception($"True State Matearial not sets");

            _renderer = GetComponent<MeshRenderer>();

            _materials[Convert.ToInt32(false)] = _falseState;
            _materials[Convert.ToInt32(true)] = _trueState;
        }

        public void ChangeMaterialByState(bool state)
        {
            _renderer.material = _materials[Convert.ToInt32(state)];
        }
    }
}

